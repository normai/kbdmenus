﻿## Issues


Issue 20220523°0851
 in [kbd23menu.py](./../py/kbd23menu.py), which is using the `tkinter` module.
 If you leave the console window, switching to some other window,
 then come back, then the console will be unresponsive. Check - What
 can be done about? Is the focus inside the hidden GUI window?


&nbsp;

<sup><sub><sup>*[file 20220203°1224]* ܀Ω</sup></sub></sup>
