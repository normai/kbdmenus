﻿## Changelog

v0.1.2 — 20220525°1121 — Add three Python files

v0.1.1 — 20220204°1717 — Two C++ flavours

v0.1.0 — 20220204°1232 — Initial

<sup><sub><sup>*[file 20220203°1223]* ܀Ω</sup></sub></sup>
