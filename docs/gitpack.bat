@echo off
rem file 20220204`1726 (after 20220123`0821)
rem See https://stackoverflow.com/questions/35410336/how-to-compact-local-git-repo [ref 20220123`0812]
rem summary : (1) Expire unreachable content and pack repo (2) Clear reflog

color 2f
title This is %~n0%~x0 by user %USERNAME%
echo ***************************************************************
echo *** ThisDrive = %~d0
echo *** ThisDir   = %~dp0
echo *** CWD       = %cd%
echo *** Params    = %1 %2 %3
echo ***************************************************************

%~d0
cd %~dp0
@echo on

git gc --aggressive --prune=now
git reflog expire --expire=now --all

pause
