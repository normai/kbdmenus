﻿// file : 20220203°1231 www.trekta.biz/svn/demoscpp/trunk/hellos/kbdmenus/cpp1kbd/cpp1kbd.cpp
// version : v0.1.1 — 20220203°1651
// summary : This demonstrates a simple console keyboard menu. Flavour:
//    • Traditonal C++ 'cin' with stream input operator, needs Enter key to be pressed
//    • Dispatcher with if/elseif controll structure
//    • Pressing CTRL-C inside debugger will throw the ugly exception
// authors : Norbert C. Maier
// copyright : © 2022 Norbert C. Maier and contributors
// license : BSD 3-Clause — See e.g. tldrlegal.com/license/bsd-3-clause-license-(revised)
// encoding : UTF-8-with-BOM

#include <iostream>
#include <string>
using namespace std;

void GoFuncA() {
   cout << "This is function A. Rattle rattle rattle .." << endl;
}

void GoFuncB() {
   cout << "This is function B. Ring chime ring .." << endl;
}

int main()
{
   cout << "*** Hello C++ keyboard menu one ***" << endl;

   // Dispatcher loop
   bool bContinue = true;
   while (bContinue) {

      // Print the reoccurring menu line, the user interface
      cout << endl << "Menu : a = Func-A, b = Func-B, x = Exit" << endl;

      // Retrieve user choice
      char cKey;
      cin >> cKey;                                     // Traditional C++ input

      cout << "   Your input was \'" << cKey << "\' = " << (int)cKey << endl;

      // Dispatcher
      if (cKey == 'a') {
         GoFuncA();
      }
      else if (cKey == 'b') {
         GoFuncB();
      }
      else if (cKey == 'x') {
         bContinue = false;
      }
      else {
         cout << "Invalid input!" << endl;
      }
   }

   cout << "Bye." << endl;
}
