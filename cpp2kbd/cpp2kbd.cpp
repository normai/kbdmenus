﻿// file : 20220203°1241 www.trekta.biz/svn/demoscpp/trunk/hellos/kbdmenus/cpp2kbd/cpp2kbd.cpp
// version : v0.1.1 — 20220203°1651
// This demonstrates a simple console keyboard menu.Flavour:
//    • Character input via the Windows specific function _getch(), delivers key immediately without waiting for Enter
//    • Pressing CTRL-C is caught and gracefully handled
//    • Dispatcher implemented with the Switch control structure
//    • Sleeps a second before closing
// authors : Norbert C. Maier
// copyright : © 2022 Norbert C. Maier and contributors
// license : BSD 3-Clause — See e.g. tldrlegal.com/license/bsd-3-clause-license-(revised)
// ref : www.cplusplus.com/reference/cstdio/getchar/ — How to get char code from keyboard press
// encoding : UTF-8-with-BOM

#include <chrono>                                      // sleep_for
#include <conio.h>                                     // Windows specific _getch()
#include <iostream>
#include <string>
#include <thread>                                      // this_thread
using namespace std;

void GoFuncA() {
   cout << "This is function A. Rattle rattle rattle .." << endl;
}

void GoFuncB() {
   cout << "This is function B. Ring chime ring .." << endl;
}

int main()
{
   cout << "*** Hello C++ keyboard menu two ***" << endl;

   // Dispatcher loop
   bool bContinue = true;
   while (bContinue) {

      // Print the reoccurring menu line, the user interface
      cout << endl << "Menu : a = Func-A, b = Func-B, x = Exit" << endl;

      // Retrieve user choice
      char cKey;
      cKey = _getch();                                 // Windows specific

      cout << "   You pressed \'" << cKey << "\' = " << (int)cKey << endl;

      // Dispatcher
      switch (cKey) {
         case 'a': GoFuncA(); break;
         case 'b': GoFuncB(); break;
         case 'x': bContinue = false; break;
         case '\3': cout << "Your key was CTRL-C" << endl; bContinue = false; break; // Secret easter egg '♥'
         default : cout << "Invalid input!" << endl;
      }
   }

   cout << "Bye." << endl;
   this_thread::sleep_for(std::chrono::milliseconds(1234));
}
