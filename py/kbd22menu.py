# License: BSD 3-Clause | © 2022 Norbert C. Maier | file 20220523°0831 kbd22menu.py
# version 20220525°1121
# Summary : Demonstrate Python keyboard menu reading keys without pressing Enter.
# Note : This is Windows specific due to using msvcrt
# ref : https://www.pythonpool.com/python-bytes-to-string/ [ref 20220525°1112]

import msvcrt                                          # Only available on Windows

print("*** Python Windows Keyboard Menu Demo ***")
bVerbose = False

while True :

   print("\nMenue: a = Action, g = Gruezi, h = Hello, v = Verbose-On/Off, x = Exit, ESC = Exit")
   bKey = msvcrt.getch()                               # Type 'Byte'
   sKey = bKey.decode()                                # Get rid of the "b'..'"

   if bVerbose :                                       # Toggle this flag to be terse/verbose
      print('Key pressed: str =', '"' + sKey + '", byte = ', '"' + str(bKey) + '",', 'type =', str(type(bKey)))  # Debug message

   # Dispatch keys
   if str(bKey) == "b'\\x1b'" :                        # ESC
      break
   elif sKey == 'a' or sKey == 'A' :
      print("Perform some action ..")
   elif sKey == 'g' or sKey == 'G' :
      print("Gruezi!")
   elif sKey == 'h' or sKey == 'H' :
      print("Hello!")
   elif sKey == 'v' or sKey == 'V' :
      bVerbose = not bVerbose                          # Toggle flag
      print("Verbosity is switched", bVerbose)
   elif sKey == 'x' or sKey == 'X' :
      print("Exit")
      break
   else :
      print("Invalid key")

print("*** Finished ***")
