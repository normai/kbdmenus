# License: BSD 3-Clause | © 2022 Norbert C. Maier | file 20220523°0821 kbd21menu.py
# version 20220525°1121
# Summary : Demonstrate simple Python keyboard menu.
# Note : This is platform independend but needs pressing Enter.

import msvcrt

print("*** Python Native Keyboard Menu Demo ***")
bVerbose = False

while True :

   sMenu ="\nMenue: a = Action, g = Gruezi, h = Hello, v = Verbose-On/Off, x = Exit, ESC = Exit : "
   sKey = input(sMenu)

   if bVerbose :                                       # Toggle this flag to be terse/verbose
      print('Key pressed:', '"' + sKey + '",', 'type =', str(type(sKey)))  # Debug message

   # Dispatch keys
   sKey = sKey.lower()
   if str(sKey) == "b'\\x1b'" :
      break
   elif sKey == 'a' :
      print("Perform some action ...")
   elif sKey == 'g' :
      print("Gruezi!")
   elif sKey == 'h' :
      print("Hello!")
   elif sKey == 'v' :
      bVerbose = not bVerbose                          # Toggle flag
      print("Verbosity is switched", bVerbose)
   elif sKey == 'x' :
      print("Exit")
      break
   else :
      print("Invalid key:", '"' + sKey + '"')

print("*** Finished ***")
