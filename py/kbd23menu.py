# License: BSD 3-Clause | © 2022 Norbert C. Maier | file 20220523°0841 kbd23menu.py
# version 20220525°1121
# Summary : Demonstrate Python keyboard menu using keys without pressing Enter.
# Note : Platform independend by using tkinter
# ref : https://www.codespeedy.com/detect-keypress-in-tkinter-in-python/
# ref : https://stackoverflow.com/questions/17280637/tkinter-messagebox-without-window
# ref : https://docs.python.org/3/library/tkinter.messagebox.html
# ref : https://pythonguides.com/python-tkinter-exit-program/
# issue : If you leave the console window, switching to some other window,
#    then come back, then the console is unresponsive. Check - What can be
#    done about? Is the focus inside the hidden GUI window? [issue 20220523°0851]

import tkinter

print("*** Python Tkinter Keyboard Menu Demo ***")
bContinue = True
bVerbose = False

def print_menu() :
   print("\nMenue: a = Action, g = Gruezi, v = Verbose-On/Off, x = Exit, ESC = Exit")

def key_pressed_event(event) :
   sKey = event.char                                   # Type = <class 'str'>

   global bContinue
   global bVerbose
   global root

   if bVerbose :                                       # Toggle this flag to be terse/verbose
      x = '-' if sKey == '' else ord(sKey)
      print('Verbosity : sKey = "' + sKey + '"', 'ord =', x)  # Debug message

   # Dispatch keys
   if sKey == '' :                                     # Catch empty string first
      print("Empty key")
   elif ord(sKey) == 27 :                              # ESC
      bContinue = False
   elif sKey == 'a' :
      print("Action")
   elif sKey == 'g' :
      print("Gruezi")
   elif sKey == 'v' :
      bVerbose = not bVerbose                          # Toggle
      print("Verbosity is switched", bVerbose)
   elif sKey == 'x' :
      print("Exit")
      bContinue = False
   else :
      print("Invalid key")

   # Prepare next round
   if bContinue :
      print_menu()
   else :
      print("*** Bye ***")
      root.destroy()

root=tkinter.Tk()                                      # Window must be created anyway ..
root.withdraw()                                        # .. but can be hidden then
root.bind("<Key>", key_pressed_event)                  # Wire event handler
print_menu()
root.mainloop()                                        # Run forever
