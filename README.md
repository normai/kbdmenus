<img src="./docs/imgs/20220204o1803.keyboard-kendo.v2.x0256y0256.png" align="right" width="256" height="256" data-dims="x500y621" style="margin-left:1.7em;" alt="Logo 'Keyboard Kendo'">

# KbdMenus &nbsp; <sup><sub><sup>*v0.1.2*</sup></sub></sup>

## Synopsis

- Slogan : Simple console keyboard menu demos in C++

- Summary : A Visual Studio solution with several projects,
 demonstrating some simple console keyboard menus

- Programming language(s) : C++ &nbsp; *(others planned)*

- License : [BSD 3-Clause](./LICENSE.txt) (explained in brief on
 [tldrlegal.com](https://tldrlegal.com/license/bsd-3-clause-license-(revised)))

- Copyright : © 2022 Norbert C. Maier and contributors

- Roadmap : Add demos for other programming languages

- Status : It Works. Other languages could be added.

## Projects

### C++

The traditional version [**cpp1kbd.cpp**](./cpp1kbd/cpp1kbd.cpp),
 which needs Enter be pressed after the selection:

- Input selection via traditional C++ **`cin` with stream input
 operator**. Needs the Enter key be pressed after the wanted character

- Dispatcher with the If-Else-If control structure

- Pressing CTRL-C will terminates without message

The Windows specific version [**cpp2kbd.cpp**](./cpp2kbd/cpp2kbd.cpp),
 without pressing Enter:

- Input selection via the **Windows specific function `_getch()`**,
   which delivers key immediately without waiting for Enter

- Pressing CTRL-C, the easter egg, is caught and gracefully handled

- Dispatcher implemented with the Switch control structure

- Sleeps a second before closing

### Python

- [**kbd21menu.py**](./py/kbd21menu.py) ─ The simplest way.
 It is just using the **`input()`** built-in function. Advantage:
 Nothing special, system idependend.
 Disadvantage: You must press Enter after each selction key.

- [**kbd22menu.py**](./py/kbd22menu.py) ─ Using the **`msvcrt`** module
 to catch the key press without waiting for Enter. Advantage:
 No need to press Enter after selection, it reacts immediately
 on each key press. Disadvantage: It is Windows specific due to
 `msvcrt` module

- [**kbd23menu.py**](./py/kbd23menu.py) ─ Using the **`tkinter`** module.
 This is more experimental, not so much for productive code.
 Advantage: Reacts immediately on each key press without waiting
 for Enter. Disadvantages: 1. You are bound to the Tkinter framework,
 which dictates the program logic. 2. There is some issue if
 you switch to another window and then come back.

---

#### Finally

This project is (also) maintained on
 [www.trekta.biz/…/kbdmenus.html](https://www.trekta.biz/svn/demoscpp/trunk/kbdmenus.html)

Find my other projects on
 [github.com/normai/](https://github.com/normai/)

Bye
 <br>Norbert
 <br>2022-May-25 &nbsp; <del>2022-Feb-04</del>


<sup><sub><sup>*[solution 20220203°1221, file 20220203°1222]* ܀Ω</sup></sub></sup>
